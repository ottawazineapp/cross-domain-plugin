This is a plugin used to get around the cross-domain problem during development.

Need to input the trusted domain name by editing the php file.

Deactivate this plugin as soon as the test is done.