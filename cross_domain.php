<?php
/*
Plugin Name: Crossdomain Solution
Plugin URI:
Description: Personal use on Ottawazine App development to get around Crossdomain problems
Version: 1.0
Author: Dianfei Xie
Author URI: www.dianfeixie.com
License:
Text Domain: Dianfei
*/

add_action( 'init', 'allow_origin' );
function allow_origin() {
    header('content-type: application/json; charset=utf-8');
    header("Access-Control-Allow-Origin: *");
}

?>
